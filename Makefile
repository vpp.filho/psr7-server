t=

init-hooks:
	git config core.hooksPath .githooks
semver:
	@sed -n 1p VERSION
up:
	docker-compose -f docker-compose.yml up
down:
	docker-compose -f docker-compose.yml down
php:
	docker exec -it mova-psr7-server-php bash

log:
	grc -c conf.log tail -f logs/app.log
install:
	composer install
update:
	composer update -i
